import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:happychat/Models/HappyResponse.dart';
import "package:http/http.dart" as http;

class RequestHandler {
  Future<HappyResponse> getData(
      {@required String? url, @required Map<String, String>? header}) async {
    Uri uri = Uri.parse(url!);
    http.Response response = await http.get(
      uri,
      headers: header,
    );
    var jsDecode = jsonDecode(response.body);
    HappyResponse happyResponse = HappyResponse.fromJson(jsDecode);
    return happyResponse;
  }

  Future<HappyResponse> postData(
      {@required String? url, @required Map<String, dynamic>? body}) async {
    Uri uri = Uri.parse(url!);
    http.Response response = await http.post(
      uri,
      body: jsonEncode(body),
    );
    var jsDecode = jsonDecode(response.body);
    HappyResponse happyResponse = HappyResponse.fromJson(jsDecode);
    return happyResponse;
  }
}
