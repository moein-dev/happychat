
class Meta {

    int? statusCode;
    bool? paginated;

    Meta();
    Meta.fromJson(Map<String, dynamic> json) {
        statusCode= json["status_code"] == null ? null : json["status_code"];
        paginated= json["paginated"] == null ? null : json["paginated"];
    }

    Map<String, dynamic> toJson() => {
        "status_code": statusCode == null ? null : statusCode,
        "paginated": paginated == null ? null : paginated,
    };
}