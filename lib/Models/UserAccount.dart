import 'package:happychat/Models/User.dart';

class UserAccount {
  DateTime? expiry;
  String? token;
  User? user;

  UserAccount();
  UserAccount.fromJson(Map<String, dynamic>? json) {
    expiry = json?["expiry"] == null ? null : DateTime.parse(json?["expiry"]);
    token = json?["token"] == null ? null : json?["token"];
    user = json?["user"] == null ? null : User.fromJson(json?["user"]);
  }

  Map<String, dynamic> toJson() => {
        "expiry": expiry == null ? null : expiry?.toIso8601String(),
        "token": token == null ? null : token,
        "user": user == null ? null : user?.toJson(),
      };
}
