import 'package:flutter/material.dart';
import 'package:happychat/res/Colors.dart';
import 'package:happychat/res/Fonts.dart';
import 'package:happychat/res/Sizing.dart';

class HappyButton extends StatelessWidget {
  final String? text;
  final double? width;
  final void Function()? onPressed;
  final bool? progressEnabled;
  HappyButton({
    Key? key,
    this.text,
    this.onPressed,
    this.width,
    this.progressEnabled,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 54,
      decoration: BoxDecoration(
        borderRadius: HappySizing.br8,
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.pressed))
                return HappyColor.grey;
              else if (states.contains(MaterialState.disabled))
                return HappyColor.grey4;
              else
                return HappyColor.black;
            },
          ),
          elevation: MaterialStateProperty.resolveWith<double>(
              (Set<MaterialState> states) {
            return 0;
          }),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: HappySizing.br8,
              side: BorderSide.none,
            ),
          ),
          padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
            (states) {
              return EdgeInsets.symmetric(vertical: 10.0);
            },
          ),
        ),
        child: progressEnabled!
            ? CircularProgressIndicator(
                color: HappyColor.white,
                strokeWidth: 1,
              )
            : Text(
                text!,
                style: TextStyle(
                  color: HappyColor.white,
                  fontFamily: HappyFont.changeFont(),
                  fontWeight: HappySizing.fw6,
                  fontSize: HappySizing.s18,
                ),
              ),
      ),
    );
  }
}
