class HappyUrls {
  static const translations = "Assets/Translations/translations.json";
  static const phoneVerification =
      "https://factor.behtarino.com/api/v1/users/phone_verification";
  static const tokenSign = "https://factor.behtarino.com/api/v1/token_sign";
  static const contactList =
      "https://factor.behtarino.com/utils/challenge/contact_list";
}
