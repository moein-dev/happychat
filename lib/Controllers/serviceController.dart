import 'dart:convert';

import 'package:get/get.dart';
import 'package:happychat/Models/HappyResponse.dart';
import 'package:happychat/Models/RoutPageModel.dart';
import 'package:happychat/Models/User.dart';
import 'package:happychat/Models/UserAccount.dart';
import 'package:happychat/Models/verification.dart';
import 'package:happychat/Utils/requestHandler.dart';
import 'package:happychat/res/urls.dart';

class ServiceController extends GetxController {
  //variables
  User _user = new User();
  List<RoutPageModel> _routPageModels = [];
  RequestHandler requestHandler = new RequestHandler();

  // getters

  User get user => _user;

  List<RoutPageModel> get routPageModels => _routPageModels;

  // setters

  set user(User user) {
    _user = user;
    update();
  }

  set routPageModels(List<RoutPageModel> routPageModels) {
    _routPageModels = routPageModels;
    update();
  }

  // functions

  Future<Verification> phoneVerification({String? phoneNumber}) async {
    String phone = "0"+ phoneNumber!.numericOnly();
    HappyResponse happyResponse =
        await requestHandler.postData(url: HappyUrls.phoneVerification, body: {
      "phone": phone,
    });
    Verification verification = new Verification();
    if (happyResponse.meta!.statusCode == 200) {
      verification = Verification.fromJson(happyResponse.data);
    }

    return verification;
  }

  Future<UserAccount> getUserData({String? code, String? phoneNumber}) async {
    HappyResponse happyResponse = await requestHandler.postData(
      url: HappyUrls.phoneVerification,
      body: {
        "username": phoneNumber,
        "password": code,
      },
    );
    UserAccount userAccount = UserAccount.fromJson(happyResponse.data);
    return userAccount;
  }

  Future<List<User>> getContacts({String? token}) async {
    HappyResponse happyResponse = await requestHandler.getData(
      url: HappyUrls.phoneVerification,
      header: {
        "Authorization": "Token $token",
      },
    );
    List<User> users = List<User>.from(
        jsonDecode("${happyResponse.data}")!.map((x) => User.fromJson(x)));

    return users;
  }
}
